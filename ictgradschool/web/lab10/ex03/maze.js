"use strict";

// TODO Your code here.
$(function animateFigure() {
    $("#figure").animate({left: "50px"}, "slow");
    $("#figure").animate({top: "30px"}, "slow");
    $("#figure").animate({left: "100px"}, "slow");
    $("#figure").animate({top: "180px"}, "slow");
    $("#figure").animate({left: "20px"}, "slow");
    $("#figure").animate({top: "230px"}, "slow");
    $("#figure").animate({left: "170px"}, "slow");
    $("#figure").animate({top: "90px"}, "slow");
    $("#figure").animate({left: "220px"}, "slow");
    $("#figure").animate({top: "240px"}, "slow");
    $("#figure").animate({left: "300px", top: "240px"}, "slow");
})

$(function animateMonster() {
    $("#monster").animate({top: "100px", left: "-100px"}, "slow").delay(200);
    $("#monster").animate({left: "30px"}, "slow");
    $("#monster").animate({top: "30px"}, "slow");
    $("#monster").animate({left: "90px"}, "slow");
    $("#monster").animate({top: "170px"}, "slow");
    $("#monster").animate({left: "20px"}, "slow");
    $("#monster").animate({top: "230px"}, "slow");
    $("#monster").animate({left: "150px"}, "slow");
    $("#monster").animate({top: "90px"}, "slow");
    $("#monster").animate({left: "220px"}, "slow");
    $("#monster").animate({top: "230px"}, "slow");
    $("#monster").animate({left: "300px", top: "240px"}, "slow");
})